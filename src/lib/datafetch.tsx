import React from "react";
import {json} from "stream/consumers";

interface Props {
    endpoint:string,
    key?:number|null,
    page?:boolean
};

export class Datafetch{
    protected fetchurl:string;
    protected url:string;
    protected endpoint:string;
    protected key:number|null;
    protected page:boolean;

    constructor(props:Props) {
        this.url = 'https://swapi.dev/api/';

        this.init(props);
        this.generateEndpoint();
    }

    protected init(props:Props) {
        //let planetlist = await fetch(process.env.API_URL as string);
        this.endpoint = props.endpoint;
        this.key = (props.key!==undefined)?props.key:null;
        this.page = (props.page!==undefined)?props.page:false;
    }

    protected generateEndpoint(){
        if( this.page )  {
            this.fetchurl = this.endpoint
        } else {
            this.fetchurl = this.url+this.endpoint+'/';
            if (typeof this.key==="number") this.fetchurl = this.fetchurl+this.key+'/';
        }
    }

    public getFetchUrl(){
        return this.fetchurl;
    }

    public async fetchApiData(){
        try {
            let response = await fetch(this.fetchurl)
            if (response.ok) {
                return response.json();
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }
}
