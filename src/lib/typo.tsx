export class Typo{
    public changeTitle(title:string):string {
        let p = title.replace('_', ' ');
        return p.charAt(0).toUpperCase()+ p.slice(1);
    }

    public changeArray(data:string|[]):string {
        if(typeof data[1]==="string") {
            return data[1];
        } else {
            let newdata = '';
            let datamap = data[1] as [];
            datamap.map((item, index) => (
                newdata = newdata +  datamap[index] + '\n'
            ))
            return newdata;
        }
    }

    public returnArray(data:string|[]) {
        if(typeof data[1]==="string") {
            return data[1];
        } else {
            let newdata = '';
            let datamap = data[1] as [];
            return datamap;
        }
    }

    public formatNumbers(labelValue:string):string{
            return Math.abs(Number(labelValue)) >= 1.0e+9
                ? Math.abs(Number(labelValue)) / 1.0e+9 + 'B'
                : Math.abs(Number(labelValue)) >= 1.0e+6
                    ? Math.abs(Number(labelValue)) / 1.0e+9 + 'M'
                    : Math.abs(Number(labelValue)) >= 1.0e+3
                        ? Math.abs(Number(labelValue)) / 1.0e+3 + 'K'
                        : labelValue
    }

    public formatDate(date:string){
        return new Date(date)
    }
}

