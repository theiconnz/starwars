import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom"
import {Datafetch} from "../../lib/datafetch";
import {Typo} from "../../lib/typo";

export const Details = (props:object) => {
    const [title, setTitle] = useState<string>();
    const [state, setState] = useState<boolean>(false);
    const [list, setList] = useState([]);
    const [showpeople, setShowpeople] = useState<boolean>(false);
    const [showfilms, setShowfilms] = useState<boolean>(false);

    const params = useParams();
    const navigate = useNavigate();
    const planetid =  parseInt(params.planetid);
    const typo = new Typo();

    const init = async () => {
        let options = {
            endpoint:"planets",
            key: planetid
        }
        let dataModel = new Datafetch(options);
        let planet = await dataModel.fetchApiData();

        setList(Object.entries(planet));
        setTitle("Planet "+ planet.name)
        setState(true);
    }

    const getApiData= async (endpoint:string) => {
        let options = {
            endpoint:endpoint,
            page: true
        }
        let dataModel = new Datafetch(options);
        let apiresults = await dataModel.fetchApiData();
        return apiresults;
    }

    const updateList = async (link:string, endpoint:string) => {
        var regex = /(.*)/;
        var linkmap = '/planets/';

        switch(endpoint) {
            case "url":
                regex = /https:\/\/swapi.dev\/api\/planets\/([0-9]*)\//g;
                linkmap = '/planets/';
                break;
            case "films":
                regex = /https:\/\/swapi.dev\/api\/films\/([0-9]*)\//g;
                linkmap = '/films/';
                break;
            default:
                regex = /https:\/\/swapi.dev\/api\/people\/([0-9]*)\//g;
                linkmap = '/people/';
        }

        let linkid = regex.exec(link);
        let newlink = linkmap+linkid[1];alert(newlink);
        navigate(newlink)
    }

    const ShowArray = (props:any) => {
        var dp = typo.returnArray(props.data);


        if(typeof dp==="string") {
            return <ul className="align-middle"><li>{dp}</li></ul>
        } else {
            let datamap = dp;
            var showarray = true;

            switch(props.endpoint) {
                case "films":
                    showarray = false;
                    break;
                case "residents":
                    showarray = false;
                    break;
            }
            return (
                <ul className="align-middle">
                    {showarray && datamap.map((item, index) => (
                        <li>
                            <button className="page-link" onClick={() => updateList(item, props.endpoint)}>{item}</button>
                        </li>
                    ))}
                </ul>
            )
        }
    }

    const updatePeopleData = async () => {
        if(showpeople) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="residents" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {console.log(list[i][1][j]);
                        let x = await getApiData(list[i][1][j]);
                        let xcontent = "Name : " + x.name + " | Gender: " + x.gender + " ";
                        const res = document.getElementById("key-residents") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowpeople(true);
            }
        }
    }


    const updateFilmsData = async () => {
        if(showfilms) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="films" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent = "Movie : " + x.title + " | Director: " + x.director + " ";
                        const res = document.getElementById("key-films") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowfilms(true);
            }
        }
    }

    useEffect(() => {
        if(!state) init();
        if(!showpeople) updatePeopleData();
        if(!showfilms) updateFilmsData();
        setState(true)
    }, [list]);


    return (
        <>
            <div className="container-fluid align-middle h-100 text-center m-auto">
                <h1 className="text-center">{title}</h1>

                <table className="table table-dark table-hover swapitable">
                    <tbody>
                    { list.length > 0
                        ? list.map((item, index) => (
                                <tr key={"key"+index}>
                                <td className="col-4 align-middle">{typo.changeTitle(item[0])}</td>
                                <td className="col-8 align-middle" id={"key-"+item[0]}>
                                    <ShowArray data={item} endpoint={item[0]} />
                                </td>
                            </tr>
                        ))
                        : <tr>
                            <td>No data found</td>
                        </tr>
                    }
                    </tbody>
                </table>
            </div>
        </>
    );
}