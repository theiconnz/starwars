import React from "react";

interface Props {
    next:string|null,
    previous:string|null
};


export class Pagination extends React.Component<Props>{
    private nextpage:string|null=null;
    private prevpage:string|null=null;

    constructor(props:Props) {
        super(props);

        this.nextpage = props.next;
        this.prevpage = props.previous;
    }

    linkPrev() {
        if(typeof this.prevpage==="string") {
            return <li className="page-item"><a className="page-link" href={this.prevpage}>Previous</a></li>
        }
        return;
    }

    linkNext() {
        if(typeof this.nextpage==="string") {
            return <li className="page-item"><a className="page-link" href={this.nextpage}>Next</a></li>
        }
        return;
    }


    render() {
        return (
            <>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        {this.linkPrev()}
                        {this.linkPrev()}
                    </ul>
                </nav>
            </>
        );
    }
}
