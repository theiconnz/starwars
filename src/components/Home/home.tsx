import React from "react";
import {Link} from "react-router-dom";


export class Homepage extends React.Component{
    render() {
        return (
            <>
                <div className="container h-100 mt-5">
                    <div className="row align-items-center gx-5">
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/people">
                                <img src="/images/people.png" className="rounded-circle img-fluid" alt="People" />
                                <div className="p-3">People</div>
                            </Link>
                        </div>
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/planets">
                                <img src="/images/planets.png" className="rounded-circle img-fluid" alt="Planets" />
                                <div className="p-3">Planets</div>
                            </Link>
                        </div>
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/species">
                                <img src="/images/species.png" className="rounded-circle img-fluid" alt="Species" />
                                <div className="p-3">Species</div>
                            </Link>
                        </div>
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/films">
                                <img src="/images/films.png" className="rounded-circle img-fluid" alt="Films" />
                                <div className="p-3">Films</div>
                            </Link>
                        </div>
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/vehicles">
                                <img src="/images/vehicles.png" className="rounded-circle img-fluid" alt="Vehicles" />
                                <div className="p-3">Vehicles</div>
                            </Link>
                        </div>
                        <div className="col text-center col-sm-6 col-md-4 col-lg-4">
                            <Link to="/starships">
                                <img src="/images/starships.png" className="rounded-circle img-fluid" alt="Starships" />
                                <div className="p-3">Starships</div>
                            </Link>
                        </div>
                   </div>
                </div>
            </>
        );
    }
}
