import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";
import './navigation.scss'


export default function Navigation() {
    const [isActive, setIsActive] = useState(false);
    const location = useLocation();

    const handleClick = () => {
        // 👇️ toggle isActive state on click
        setIsActive(current => !current);
    };

    React.useEffect(() =>{
        if(isActive){
            handleClick();
        }
    }, [location]);


    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light swapi-navigation navbar-brand">
                <div className="container-fluid">
                    <Link to="/" className="nav-link">
                        <img src="/images/rebel.jpg" className="rounded-circle img-fluid" alt="Planets" width="50px" />
                    </Link>
                    <button className="navbar-toggler" type="button"
                            onClick={() => {
                                setIsActive(!isActive)
                            }}
                        >
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className={`${ isActive ? 'navigation-menu-expanded' : 'navigation-menu'} bg-light navbar-collapse`} id="swapinav">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item m-2">
                                <Link to="/" className="nav-link">Home</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/people" className="nav-link">People</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/planets" className="nav-link">Planets</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/species" className="nav-link">Species</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/vehicles" className="nav-link">Vehicles</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/starships" className="nav-link">Starships</Link>
                            </li>
                            <li className="nav-item m-2">
                                <Link to="/films" className="nav-link">Films</Link>
                            </li>
                        </ul>
                    </div>

                    <div className="d-none .d-sm-block">

                    </div>
                </div>
            </nav>
        </>
    );
}
