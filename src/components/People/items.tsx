import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

type Props = {
    list: any[],
    endpoint?:string,
    replacelink?:string
};

const Items = (props:Props) => {
    const navigate = useNavigate();
    const result = Object.values(props.list);

    const redirecto = (link:string) => {
        let regex = /https:\/\/swapi.dev\/api\/people\/([0-9]*)\//g;
        let linkid = regex.exec(link);
        let newlink = linkid[1];
        navigate(newlink)
    }


    const content = result.map((result: any, index) =>
        <tr data-url={result.url} key={result.url} onClick={() => redirecto(result.url)}>
            <td>{result.name}</td>
            <td className="text-right">{result.gender}</td>
            <td className="text-right">{result.height}</td>
        </tr>
    );


    return (
        <>
            {content}
        </>
    );
}

export default Items;