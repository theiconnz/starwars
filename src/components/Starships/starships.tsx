import React, { useEffect, useState } from "react";
import {Pagination} from "../Pagination/pagination";
import { Datafetch } from "../../lib/datafetch";
import Items from "./items"

const Starships = () => {
    const [list, setList] = useState([]);
    const [nextp, setNextp] = useState<string|null>();
    const [prevp, setPrevp] = useState<string|null>();
    const [title, setTitle] = useState<string>();

    const init = async () => {
        let options = {
            endpoint:"starships"
        }
        let dataModel = new Datafetch(options);
        let star = await dataModel.fetchApiData();
        setNextp(star.next);
        setPrevp(star.previous);
        setList(star.results);
        setTitle('Starships');
    }

    const updateList = async (endpoint:string|null) => {
        if(typeof endpoint==="string"){
            let options = {
                endpoint:endpoint,
                page: true
            }
            let dataModel = new Datafetch(options);
            let star = await dataModel.fetchApiData();
            setNextp(star.next);
            setPrevp(star.previous);
            setList(star.results);
            setTitle('Starships');
        }
    }


    useEffect(() => {
        init();
    }, []);



    return (
        <>
            <div>

                <h1 className="text-center">{title}</h1>
                <p>Click individual row to view more details</p>
                <table className="table table-dark table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">model</th>
                        <th scope="col">Class</th>
                    </tr>
                    </thead>
                    <tbody>
                    <Items list={list} />
                    </tbody>
                </table>

                {list.length>0
                    ? <nav aria-label="Page navigation example">
                        <ul className="pagination justify-content-end">
                            <li className={`${
                                prevp===null ? 'disabled' : ''
                            } page-item`}>
                                <button className="page-link" onClick={() => updateList(prevp)}>Previous</button>
                            </li>
                            <li className={`${
                                nextp===null ? 'disabled' : ''
                            } page-item`}>
                                <button className="page-link" onClick={() => updateList(nextp)}>Next</button>
                            </li>
                        </ul>
                    </nav>
                    : ''
                }
            </div>
        </>
    );

}

export default Starships;
