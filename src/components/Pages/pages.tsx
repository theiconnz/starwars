import React, { useEffect, useState } from "react";


export const NoMatch = () => {
    return (
        <>
            <div className="container-fluid align-middle h-100 text-center m-auto">
                404 Page not found
            </div>
        </>
    );
}