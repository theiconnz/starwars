import React, { useEffect, useState } from "react";
import {Pagination} from "../Pagination/pagination";
import { Datafetch } from "../../lib/datafetch";
import Items from "./items"

const Films = () => {
    const [list, setList] = useState([]);
    const [nextp, setNextp] = useState<string|null>();
    const [prevp, setPrevp] = useState<string|null>();
    const [title, setTitle] = useState<string>();

    const init = async () => {
        let options = {
            endpoint:"films"
        }
        let dataModel = new Datafetch(options);
        let planets = await dataModel.fetchApiData();
        setNextp(planets.next);
        setPrevp(planets.previous);
        setList(planets.results);
        setTitle('Films');
    }

    const updateList = async (endpoint:string|null) => {
        if(typeof endpoint==="string"){
            let options = {
                endpoint:endpoint,
                page: true
            }
            let dataModel = new Datafetch(options);
            let planets = await dataModel.fetchApiData();
            setNextp(planets.next);
            setPrevp(planets.previous);
            setList(planets.results);
            setTitle('Films');
        }
    }


    useEffect(() => {
        init();
    }, []);



    return (
        <>
            <div>

                <h1 className="text-center">{title}</h1>

                <p>Click individual row to view more details</p>
                <table className="table table-dark table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Director</th>
                        <th scope="col">Release Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <Items list={list} />
                    </tbody>
                </table>

                {list.length>0
                    ? <nav aria-label="Page navigation example">
                        <ul className="pagination justify-content-end">
                            <li className={`${
                                prevp===null ? 'disabled' : ''
                            } page-item`}>
                                <button className="page-link" onClick={() => updateList(prevp)}>Previous</button>
                            </li>
                            <li className={`${
                                nextp===null ? 'disabled' : ''
                            } page-item`}>
                                <button className="page-link" onClick={() => updateList(nextp)}>Next</button>
                            </li>
                        </ul>
                    </nav>
                    : ''
                }
            </div>
        </>
    );

}

export default Films;
