import React, {useEffect, useState} from "react";
import { useNavigate, useParams} from "react-router-dom"
import {Datafetch} from "../../lib/datafetch";
import {Typo} from "../../lib/typo";

export const FilmsDetails = (props:object) => {
    const [title, setTitle] = useState<string>();
    const [state, setState] = useState<boolean>(false);
    const [list, setList] = useState([]);
    const [showpeople, setShowpeople] = useState<boolean>(false);
    const [showstarships, setShowstarships] = useState<boolean>(false);
    const [showplanets, setShowplanets] = useState<boolean>(false);
    const [showvehicles, setShowvehicles] = useState<boolean>(false);
    const [showspecies, setShowspecies] = useState<boolean>(false);

    const params = useParams();
    const navigate = useNavigate();
    const filmid =  parseInt(params.filmid);
    const typo = new Typo();

    const init = async () => {
        let options = {
            endpoint:"films",
            key: filmid
        }
        let dataModel = new Datafetch(options);
        let films = await dataModel.fetchApiData();

        setList(Object.entries(films));
        setTitle( films.title)
        setState(true);
    }

    const getApiData= async (endpoint:string) => {
        let options = {
            endpoint:endpoint,
            page: true
        }
        let dataModel = new Datafetch(options);
        let films = await dataModel.fetchApiData();
        return films;
    }

    const updateList = async (link:string, endpoint:string) => {
        var regex = /(.*)/;
        var linkmap = '/planets/';

        switch(endpoint) {
            case "planets":
                regex = /https:\/\/swapi.dev\/api\/planets\/([0-9]*)\//g;
                linkmap = '/planets/';
                break;
            case "starships":
                regex = /https:\/\/swapi.dev\/api\/starships\/([0-9]*)\//g;
                linkmap = '/starships/';
                break;
            default:
                regex = /https:\/\/swapi.dev\/api\/people\/([0-9]*)\//g;
                linkmap = '/people/';
        }

        let linkid = regex.exec(link);
        let newlink = linkmap+linkid[1];alert(newlink);
        navigate(newlink)
    }

    const ShowArray = (props:any) => {
        var dp = typo.returnArray(props.data);
        var showarray = true;

        switch(props.endpoint) {
            case "starships":
                showarray = false;
                break;
            case "characters":
                showarray = false;
                break;
            case "planets":
                showarray = false;
                break;
            case "vehicles":
                showarray = false;
                break;
            case "species":
                showarray = false;
                break;
        }

        if(typeof dp==="string" && showarray) {
            return <ul className="align-middle"><li>{dp}</li></ul>
        } else if(typeof dp==="string" && !showarray){
            return <div></div>;
        } else if(typeof dp!=="string" && !showarray){
            let datamap = dp;

            return (
                <ul className="align-middle">
                    {showarray && datamap.map((item, index) => (
                        <li>
                            <button className="page-link" onClick={() => updateList(item, props.endpoint)}>{item}</button>
                        </li>
                    ))}
                </ul>
            )
        }
    }

    const updatePeopleData = async () => {
        if(showpeople) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="characters" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent =  x.name + " | Gender: " + x.gender + " ";
                        const res = document.getElementById("key-characters") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowpeople(true);
            }
        }
    }

    const updatePlanetsData = async () => {
        if(showplanets) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="planets" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent =  x.name + " | Population: " + typo.formatNumbers( x.population) + " ";
                        const res = document.getElementById("key-planets") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowplanets(true);
            }
        }
    }

    const updateStarshipsData = async () => {
        if(showstarships) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="starships" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent = x.name + " | Model: " + x.model + " ";
                        const res = document.getElementById("key-starships") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowstarships(true);
            }
        }
    }

    const updateSpeciesData = async () => {
        if(showspecies) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="species" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent = "Name : " + x.name + " | Classification: " + x.classification + " ";
                        const res = document.getElementById("key-species") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowspecies(true);
            }
        }
    }

    const updateVehicleData = async () => {
        if(showvehicles) return;

        for (var i = 0; i < list.length; i++) {
            if(list[i][0]=="vehicles" && list[i][1].length>0) {
                for (var j = 0; j < list[i][1].length; j++) {
                    if(j<50) {
                        let x = await getApiData(list[i][1][j]);
                        let xcontent =  x.name + " | Model: " + x.model + " ";
                        const res = document.getElementById("key-vehicles") as HTMLTableCellElement;
                        const div = document.createElement("div");
                        const newContent = document.createTextNode(xcontent);
                        div.appendChild(newContent);
                        res.appendChild(div);
                    }
                }
                setShowvehicles(true)
            }
        }
    }


    useEffect(() => {
        if(!state) init();
        if(!showpeople) updatePeopleData();
        if(!showstarships) updateStarshipsData();
        if(!showplanets) updatePlanetsData();
        if(!showvehicles) updateVehicleData();
        if(!showspecies) updateSpeciesData();
        setState(true)
    }, [list]);


    return (
        <>
            <div className="container-fluid align-middle h-100 text-center m-auto">
                <h1 className="text-center">{title}</h1>

                <table className="table table-dark table-hover swapitable">
                    <tbody>
                    { list.length > 0
                        ? list.map((item, index) => (
                                <tr key={"key"+index}>
                                <td className="col-4 align-middle">{typo.changeTitle(item[0])}</td>
                                <td className="col-8 align-middle" id={"key-"+item[0]}>
                                    <ShowArray data={item} endpoint={item[0]} />
                                </td>
                            </tr>
                        ))
                        : <tr>
                            <td>No data found</td>
                        </tr>
                    }
                    </tbody>
                </table>
            </div>
        </>
    );
}