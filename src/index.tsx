import React from 'react';
import ReactDOM from 'react-dom/client';
// Import our custom CSS
import './scss/styles.scss'
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {Homepage} from "./components/Home/home";
import Planets from "./components/Planets/planets";
import People from "./components/People/people";
import Species from "./components/Species/species";
import Navigation from "./components/Navigation/navigation";
import {NoMatch} from "./components/Pages/pages";
import {Details} from "./components/Planets/details";
import {PeopleDetails} from "./components/People/details";
import {SpeciesDetails} from "./components/Species/details";
import Vehicles from "./components/Vehicles/vehicles";
import {VehicleDetails} from "./components/Vehicles/details";
import Starships from "./components/Starships/starships";
import {StarshipDetails} from "./components/Starships/details";
import Films from "./components/Films/films";
import {FilmsDetails} from "./components/Films/details";


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
    <div className="App">
        <div className="container-md">
            <Router>
                <Navigation />

                <Routes>
                    <Route path="/" element={<Homepage />} />
                    <Route path="/planets" element={<Planets />} />
                    <Route path="/planets/:planetid" element={<Details />} />
                    <Route path="/people" element={<People />} />
                    <Route path="/people/:personid" element={<PeopleDetails />} />
                    <Route path="/species" element={<Species />} />
                    <Route path="/species/:speciesid" element={<SpeciesDetails />} />
                    <Route path="/vehicles" element={<Vehicles />} />
                    <Route path="/vehicles/:vehicleid" element={<VehicleDetails />} />
                    <Route path="/starships" element={<Starships />} />
                    <Route path="/starships/:shipid" element={<StarshipDetails />} />
                    <Route path="/films" element={<Films />} />
                    <Route path="/films/:filmid" element={<FilmsDetails />} />
                    <Route path="*" element={<NoMatch />} />
                </Routes>
            </Router>
        </div>
    </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
